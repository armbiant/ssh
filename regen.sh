#!/bin/bash

source "$FUCHSIA_DIR"/tools/devshell/lib/vars.sh

set -euxo pipefail

fx set core.qemu-x64 --args=build_sdk_archives=true --auto-dir
fx build sdk:zircon_sysroot

readonly BUILD_DIR="$FUCHSIA_OUT_DIR"/core.qemu-x64
readonly SYSROOT_ROOT=sysroot
readonly SYSROOT_DIR=$SYSROOT_ROOT/arch/x64/sysroot
readonly TARGET=x86_64-unknown-fuchsia

mkdir -p $SYSROOT_ROOT
tar -C $SYSROOT_ROOT -xf $BUILD_DIR/sdk/archive/zircon_sysroot.tar.gz || true

for name in config.{guess,sub}; do
  wget -O $name "https://git.savannah.gnu.org/gitweb/?p=config.git;a=blob_plain;f=$name;hb=HEAD"
done

autoreconf
./configure \
  --host $TARGET \
  --disable-{pkcs11,security-key,lastlog,utmp,utmpx,wtmp,wtmpx,libutil,pututline,pututxline} \
  --with-default-path=/bin:/pkg/bin:/boot/bin:/system/bin \
  --without-openssl-header-check \
  CC="$PREBUILT_CLANG_DIR"/bin/clang \
  CFLAGS="-target $TARGET -nostdinc -nostdlib" \
  CPPFLAGS="-I$SYSROOT_DIR/include -I$PREBUILT_CLANG_DIR/lib/clang/11.0.0/include -I$FUCHSIA_DIR/third_party/boringssl/src/include -I$FUCHSIA_DIR/third_party/zlib" \
  LDFLAGS="-L$SYSROOT_DIR/lib -L$BUILD_DIR/x64-shared -lc"

# Manually override some symbols we expose but don't implement.
for i in HAVE_{FLOCK,GETSID,SETEUID,SETGROUPS}; do
  sed -i "s,^#define $i 1$,/* #undef $i */," config.h
done
mv config.h fuchsia
