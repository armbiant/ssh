// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "third_party/openssh-portable/fuchsia/loader-wrapper.h"

#include <fidl/fuchsia.io/cpp/wire.h>
#include <lib/zx/result.h>
#include <zircon/errors.h>

#include <fbl/unique_fd.h>
#include <src/lib/loader_service/loader_service.h>

uint32_t lib_dir_open_flags() {
  return static_cast<uint32_t>(fuchsia_io::wire::OpenFlags::kRightReadable |
                               fuchsia_io::wire::OpenFlags::kRightExecutable |
                               fuchsia_io::wire::OpenFlags::kDirectory);
}

struct loader_service {
  std::shared_ptr<loader::LoaderService> loader;
};

zx_status_t loader_service_create(async_dispatcher_t* dispatcher, int lib_dir_fd, const char* name,
                                  loader_service_t** out) {
  if (!out) {
    return ZX_ERR_INVALID_ARGS;
  }

  loader_service_t* svc = new loader_service_t;
  if (!svc) {
    return ZX_ERR_NO_MEMORY;
  }
  svc->loader = loader::LoaderService::Create(dispatcher, fbl::unique_fd(lib_dir_fd), name);
  *out = svc;
  return ZX_OK;
}

zx_status_t loader_service_connect(loader_service_t* svc, zx_handle_t* out) {
  auto status = svc->loader->Connect();
  if (status.is_error()) {
    return status.status_value();
  }
  *out = status->TakeChannel().release();
  return ZX_OK;
}

zx_status_t loader_service_release(loader_service_t* svc) {
  delete svc;
  return ZX_OK;
}
