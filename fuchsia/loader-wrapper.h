// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef THIRD_PARTY_OPENSSH_PORTABLE_FUCHSIA_LOADER_WRAPPER_H_
#define THIRD_PARTY_OPENSSH_PORTABLE_FUCHSIA_LOADER_WRAPPER_H_

#include <lib/async/dispatcher.h>
#include <zircon/compiler.h>

__BEGIN_CDECLS

// C wrapper for C++ loader::LoaderService class.

typedef struct loader_service loader_service_t;

uint32_t lib_dir_open_flags();

// Create a new file-descriptor backed fuchsia.ldsvc.Loader service capable of handling any number
// of clients.
//
// Requests will be processed on the given |dispatcher|. Paths and objects will be loaded relative
// to |lib_dir_fd|, and the loader service will take ownership of |lib_dir_fd|.
//
// If the returned status is ZX_OK, the caller must call loader_service_release on the returned
// loader_service_t to avoid leaks. On error out is not written.
zx_status_t loader_service_create(async_dispatcher_t* dispatcher, int lib_dir_fd, const char* name,
                                  loader_service_t** out);

// Returns a new channel connected to the loader service, owned by the caller.
zx_status_t loader_service_connect(loader_service_t* svc, zx_handle_t* out);

// After this function returns, the loader service will stop serving once there are no longer any
// outstanding connections. loader_service_connect should not be called on a given loader_service_t
// after calling _release (even if there are outstanding connections).
zx_status_t loader_service_release(loader_service_t* svc);

__END_CDECLS

#endif  // THIRD_PARTY_OPENSSH_PORTABLE_FUCHSIA_LOADER_WRAPPER_H_
