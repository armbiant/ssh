# OpenSSH on Fuchsia

## Server

Fuchsia builds that include `sshd` start a program called `listen` that is
similar in nature to `inetd` from other platforms, in that `listen` takes the
responsibility to listen on a socket and spawn programs when connections are
made. `listen` is configured to spawn `sshd` when it receives a connection on
port 22 over either IPv4 or IPv6.

The sshd is configured such that it expects a username of `fuchsia`.

## Host Keys

There is a minimal program called `hostkeygen` that is also run on startup,
that will create a host private key file in `/data/ssh/` if none is already
present.

## User Keys

The `jiri` tool is configured to run a script on checkout that provisions a
user specific SSH key into `$FUCHSIA_DIR/.ssh/pkey` and an associated
authorized_keys file into `$FUCHSIA_DIR/.ssh/authorized_keys`. Users are free
to replace the contents of the authorized_keys file with their preferred keys
if they desire, for example to add keys from your ssh-agent, one might
invoke: `ssh-add -L > $FUCHSIA_DIR/.ssh/authorized_keys`.

User keys can be provisioned to Fuchsia devices in several ways:

* During "paving", using the `bootserver` tools `--authorized-keys` argument
  (`fx pave` does this).
* By embedding keys into the build, using the `extra_authorized_keys` build
  argument. This approach is not recommended, as the image artifacts produced
  by a build in this case are by side effect specialized to the given keys.
* By some usual on-target means, such as fetching the keys from Github:
  `curl -o /data/ssh/authorized_keys https://github.com/<username>.keys`

## Notes

* There is no termcap on Fuchisa, and the shell (a Dash fork) has limited
  knowledge of terminals. It is recommended that you use a terminal that is
  loosely vt100/xterm compatible.
* The SSH client feature `-N` typically used to background a connection does
  not currently work. The workaround is to run a long running program such as
  `cat`.
* Fuchsia supports a `ignore_valid_after` configuration option to ignore the
  `valid_after` date check when authenticating via certificate. This is used
  during development to connect to devices that do not have a synchronized time
  source. See https://fxrev.dev/302848 for details.
* Fuchsia does not support password authentication.

## Client

The clients are built but don't work yet.

## Updating

To update Fuchsia's fork to a new version of openssh-portable perform the following steps:

1. Identify a released version of openssh-portable and find the corresponding
tag prefixed by 'upstream' in our mirror.
2. Merge changes from upstream into origin/main and resolve merge conflicts.
3. Compare the build rules in Makefile.in and reconcile changes in BUILD.gn.
4. Compare changes in the autoconf configuration and update fuchsia/config.h.
