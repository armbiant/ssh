// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include <fidl/fuchsia.hardware.pty/cpp/wire.h>
#include <lib/fdio/cpp/caller.h>
#include <lib/fdio/directory.h>
#include <lib/fdio/fd.h>
#include <lib/fdio/io.h>
#include <lib/fdio/unsafe.h>
#include <lib/zx/channel.h>
#include <stdio.h>
#include <zircon/status.h>
#include <zircon/syscalls.h>

#include <utility>

#include "fuchsia-compat.h"

namespace fpty = fuchsia_hardware_pty;

int fuchsia_open_pty_client(int fd, uint32_t id) {
  fdio_cpp::UnownedFdioCaller caller(fd);

  if (!caller) {
    fprintf(stderr, "Failed to open PTY client: couldn't create fdio\n");
    return -1;
  }

  auto endpoints = fidl::CreateEndpoints<fpty::Device>();
  if (endpoints.is_error()) {
    fprintf(stderr, "Failed to open PTY client: %s\n", endpoints.status_string());
    return -1;
  }
  auto result = fidl::WireCall(caller.borrow_as<fpty::Device>())
                    ->OpenClient(id, std::move(endpoints->server));

  if (result.status() != ZX_OK) {
    fprintf(stderr, "Failed to open PTY client: %s\n", zx_status_get_string(result.status()));
    return -1;
  }
  if (result.value().s != ZX_OK) {
    fprintf(stderr, "Failed to open PTY client: %s\n", zx_status_get_string(result.value().s));
    return -1;
  }

  int client_fd;
  zx_status_t status = fdio_fd_create(endpoints->client.channel().release(), &client_fd);
  if (status != ZX_OK) {
    fprintf(stderr, "Failed to open PTY client: %s\n", zx_status_get_string(status));
    return -1;
  }
  return client_fd;
}
