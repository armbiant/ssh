// Copyright 2017 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef THIRD_PARTY_OPENSSH_PORTABLE_FUCHSIA_FUCHSIA_COMPAT_H_
#define THIRD_PARTY_OPENSSH_PORTABLE_FUCHSIA_FUCHSIA_COMPAT_H_

#include <stdint.h>
#include <zircon/compiler.h>

__BEGIN_CDECLS

#define SSHDIR "/pkg/data/ssh"

// We don't actually have these functions, but the compatibility shims don't compile with BoringSSL.
// At the time of writing, only pkcs11 uses these functions, and we disable it.
#define HAVE_RSA_METH_FREE 1
#define HAVE_RSA_METH_DUP 1
#define HAVE_RSA_METH_SET1_NAME 1
#define HAVE_RSA_METH_SET_PRIV_ENC 1
#define HAVE_RSA_METH_SET_PRIV_DEC 1

#define CUSTOM_SYS_AUTH_PASSWD 1

struct timeval;

void fuchsia_init_async(void);

int chroot(const char* path);

// Note: char** env is consumed by this function and will be freed.
int fuchsia_launch_child(const char* command, char** env, int in, int out, int err);

// Open a PTY client of |fd|, where |fd| is either a PTY server or controlling
// client. The |id| must be unique for the server. The client where |id| is 0 is
// the controlling client. Returns a file descriptor for the newly opened
// client.
int fuchsia_open_pty_client(int fd, uint32_t id);

__END_CDECLS

#endif  // THIRD_PARTY_OPENSSH_PORTABLE_FUCHSIA_FUCHSIA_COMPAT_H_
