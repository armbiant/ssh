// Copyright 2018 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// hostkeygen is a minimalist program to generate host keys iff needed.
#include "includes.h"

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "authfile.h"
#include "ssherr.h"
#include "sshkey.h"

// Deallocate on each program exit to fix memory leak findings.
static void sshkey_auto_free(struct sshkey** key) { sshkey_free(*key); }

int main() {
  char* key_type = "ed25519";
  char* path = "/data/ssh/ssh_host_ed25519_key";
  char* dir = "/data/ssh";

  int type, r;
  struct stat st;

  __attribute__((cleanup(sshkey_auto_free))) struct sshkey* private = NULL;
  __attribute__((cleanup(sshkey_auto_free))) struct sshkey* public = NULL;

  // ignore errors, if the dir already exists, this is benign, if it does not
  // and this fails, the write will fail.
  mkdir(dir, 0700);

  if (stat(path, &st) == 0) {
    return 0;
  }

  if (errno != ENOENT) {
    perror("stat ssh private key");
    return 1;
  }

  type = sshkey_type_from_name(key_type);

  if ((r = sshkey_generate(type, 0, &private)) != 0) {
    fprintf(stderr, "sshkey_generate failed: %s\n", ssh_err(r));
    return 1;
  }

  if ((r = sshkey_from_private(private, &public)) != 0) {
    fprintf(stderr, "sshkey_from_private failed: %s\n", ssh_err(r));
    return 1;
  }

  if ((r = sshkey_save_private(private, path, "", "", 1, NULL, 0)) != 0) {
    fprintf(stderr, "Saving key \"%s\" failed: %s\n", path, ssh_err(r));
    return 1;
  }
  return 0;
}
