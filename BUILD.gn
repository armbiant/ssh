# Copyright 2017 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import("//build/components.gni")

group("openssh-tools") {
  deps = [
    ":scp-shell",
    ":ssh-keygen-shell",
    ":ssh-shell",
  ]
}

fuchsia_shell_package("scp-shell") {
  package_name = "scp"
  deps = [ ":scp" ]
}

fuchsia_shell_package("ssh-shell") {
  package_name = "ssh"
  deps = [ ":ssh" ]
}

fuchsia_shell_package("ssh-keygen-shell") {
  package_name = "ssh-keygen"
  deps = [ ":ssh-keygen" ]
}

config("fuchsia") {
  include_dirs = [
    ".",
    "fuchsia",
  ]
  cflags = [
    "-include",
    rebase_path("fuchsia/fuchsia-compat.h", root_build_dir),

    # Suppress warnings in upstream code that are triggered by Fuchsia compilation flags.
    "-Wno-conversion",
    "-Wno-extra-semi",
    "-Wno-implicit-fallthrough",
    "-Wno-pointer-sign",
    "-Wno-sign-compare",
    "-Wno-strict-prototypes",
    "-Wno-write-strings",
    "-Wno-unused-but-set-variable",
  ]

  configs = [
    "//sdk/lib/fdio:fdio.config",
    "//third_party/boringssl:boringssl_config",
    "//third_party/zlib:zlib_config",
  ]
}

source_set("fuchsia-compat") {
  sources = [
    "fuchsia/fuchsia-compat.c",
    "fuchsia/fuchsia-pty.cc",
    "fuchsia/loader-wrapper.cc",
  ]
  configs += [ ":fuchsia" ]
  deps = [
    "//sdk/fidl/fuchsia.hardware.pty:fuchsia.hardware.pty_cpp_wire",
    "//sdk/fidl/fuchsia.io:fuchsia.io_cpp_wire",
    "//sdk/lib/fdio",
    "//src/lib/loader_service",
    "//zircon/system/ulib/async-loop",
    "//zircon/system/ulib/async-loop:async-loop-default",
    "//zircon/system/ulib/fbl",
    "//zircon/system/ulib/fdio-caller",
    "//zircon/system/ulib/zircon-internal",
    "//zircon/system/ulib/zx",
  ]
}

# This needs to be a static library because some of its symbols are replaced
# when it is linked into executables.
static_library("libopenssh") {
  sources = [
    "bitmap.c",
    "krl.c",
    "ssh_api.c",
    "sshbuf-getput-basic.c",
    "sshbuf-getput-crypto.c",
    "sshbuf-misc.c",
    "sshbuf.c",
    "ssherr.c",
    "sshkey.c",
  ]
  configs += [ ":fuchsia" ]
}

# This needs to be a static library because some of its symbols are replaced
# when it is linked into executables.
static_library("libssh") {
  sources = [
    "addr.c",
    "addrmatch.c",
    "atomicio.c",
    "authfd.c",
    "authfile.c",
    "canohost.c",
    "chacha.c",
    "channels.c",
    "cipher-aes.c",
    "cipher-aesctr.c",
    "cipher-chachapoly-libcrypto.c",
    "cipher-chachapoly.c",
    "cipher.c",
    "cleanup.c",
    "compat.c",
    "dh.c",
    "digest-libc.c",
    "digest-openssl.c",
    "dispatch.c",
    "dns.c",
    "ed25519.c",
    "entropy.c",
    "fatal.c",
    "fe25519.c",
    "ge25519.c",
    "gss-genr.c",
    "hash.c",
    "hmac.c",
    "hostfile.c",
    "kex.c",
    "kexc25519.c",
    "kexdh.c",
    "kexecdh.c",
    "kexgen.c",
    "kexgex.c",
    "kexgexc.c",
    "kexgexs.c",
    "kexsntrup761x25519.c",
    "log.c",
    "mac.c",
    "match.c",
    "misc.c",
    "moduli.c",
    "monitor_fdpass.c",
    "msg.c",
    "nchan.c",
    "packet.c",
    "platform-misc.c",
    "platform-pledge.c",
    "platform-tracing.c",
    "poly1305.c",
    "progressmeter.c",
    "readpass.c",
    "rijndael.c",
    "sc25519.c",
    "sftp-realpath.c",
    "smult_curve25519_ref.c",
    "sntrup761.c",
    "ssh-dss.c",
    "ssh-ecdsa-sk.c",
    "ssh-ecdsa.c",
    "ssh-ed25519-sk.c",
    "ssh-ed25519.c",
    "ssh-pkcs11.c",
    "ssh-rsa.c",
    "sshbuf-io.c",
    "ttymodes.c",
    "umac.c",
    "umac128.c",
    "utf8.c",
    "verify.c",
    "xmalloc.c",
  ]
  configs += [ ":fuchsia" ]
  deps = [
    ":libopenssh",
    ":openbsd-compat",
    "//third_party/boringssl",
    "//third_party/zlib:zlib_static",
  ]

  if (is_fuchsia) {
    deps += [ ":fuchsia-compat" ]
  }
}

config("openbsd-compat-config") {
  include_dirs = [ "openbsd-compat" ]
  cflags = [ "-Wno-sizeof-array-div" ]
}

source_set("openbsd-compat") {
  sources = [
    "openbsd-compat/arc4random.c",
    "openbsd-compat/arc4random_uniform.c",
    "openbsd-compat/base64.c",
    "openbsd-compat/basename.c",
    "openbsd-compat/bcrypt_pbkdf.c",
    "openbsd-compat/bindresvport.c",
    "openbsd-compat/blowfish.c",
    "openbsd-compat/bsd-asprintf.c",
    "openbsd-compat/bsd-closefrom.c",
    "openbsd-compat/bsd-cygwin_util.c",
    "openbsd-compat/bsd-err.c",
    "openbsd-compat/bsd-flock.c",
    "openbsd-compat/bsd-getentropy.c",
    "openbsd-compat/bsd-getline.c",
    "openbsd-compat/bsd-getpagesize.c",
    "openbsd-compat/bsd-getpeereid.c",
    "openbsd-compat/bsd-malloc.c",
    "openbsd-compat/bsd-misc.c",
    "openbsd-compat/bsd-nextstep.c",
    "openbsd-compat/bsd-openpty.c",
    "openbsd-compat/bsd-poll.c",
    "openbsd-compat/bsd-pselect.c",
    "openbsd-compat/bsd-setres_id.c",
    "openbsd-compat/bsd-signal.c",
    "openbsd-compat/bsd-snprintf.c",
    "openbsd-compat/bsd-statvfs.c",
    "openbsd-compat/bsd-timegm.c",
    "openbsd-compat/bsd-waitpid.c",
    "openbsd-compat/daemon.c",
    "openbsd-compat/dirname.c",
    "openbsd-compat/explicit_bzero.c",
    "openbsd-compat/fake-rfc2553.c",
    "openbsd-compat/fmt_scaled.c",
    "openbsd-compat/fnmatch.c",
    "openbsd-compat/freezero.c",
    "openbsd-compat/getcwd.c",
    "openbsd-compat/getgrouplist.c",
    "openbsd-compat/getopt_long.c",
    "openbsd-compat/getrrsetbyname-ldns.c",
    "openbsd-compat/getrrsetbyname.c",
    "openbsd-compat/glob.c",
    "openbsd-compat/inet_aton.c",
    "openbsd-compat/inet_ntoa.c",
    "openbsd-compat/inet_ntop.c",
    "openbsd-compat/kludge-fd_set.c",
    "openbsd-compat/libressl-api-compat.c",
    "openbsd-compat/md5.c",
    "openbsd-compat/memmem.c",
    "openbsd-compat/mktemp.c",
    "openbsd-compat/openssl-compat.c",
    "openbsd-compat/pwcache.c",
    "openbsd-compat/readpassphrase.c",
    "openbsd-compat/reallocarray.c",
    "openbsd-compat/recallocarray.c",
    "openbsd-compat/rresvport.c",
    "openbsd-compat/setenv.c",
    "openbsd-compat/setproctitle.c",
    "openbsd-compat/sha1.c",
    "openbsd-compat/sha2.c",
    "openbsd-compat/sigact.c",
    "openbsd-compat/strcasestr.c",
    "openbsd-compat/strlcat.c",
    "openbsd-compat/strlcpy.c",
    "openbsd-compat/strmode.c",
    "openbsd-compat/strndup.c",
    "openbsd-compat/strnlen.c",
    "openbsd-compat/strptime.c",
    "openbsd-compat/strsep.c",
    "openbsd-compat/strtoll.c",
    "openbsd-compat/strtonum.c",
    "openbsd-compat/strtoul.c",
    "openbsd-compat/strtoull.c",
    "openbsd-compat/timingsafe_bcmp.c",
    "openbsd-compat/vis.c",
  ]
  configs += [
    ":fuchsia",
    ":openbsd-compat-config",
  ]
}

source_set("sk") {
  sources = [ "ssh-sk-client.c" ]
  configs += [ ":fuchsia" ]
}

source_set("sftp-client") {
  sources = [
    "sftp-client.c",
    "sftp-common.c",
    "sftp-glob.c",
  ]
  configs += [ ":fuchsia" ]
}

executable("ssh") {
  sources = [
    "clientloop.c",
    "mux.c",
    "readconf.c",
    "ssh.c",
    "sshconnect.c",
    "sshconnect2.c",
    "sshtty.c",
  ]
  configs += [ ":fuchsia" ]
  deps = [
    ":libssh",
    ":sk",
  ]
}

executable("sshd") {
  sources = [
    "audit-bsm.c",
    "audit-linux.c",
    "audit.c",
    "auth-bsdauth.c",
    "auth-krb5.c",
    "auth-options.c",
    "auth-pam.c",
    "auth-passwd.c",
    "auth-rhosts.c",
    "auth-shadow.c",
    "auth-sia.c",
    "auth.c",
    "auth2-chall.c",
    "auth2-gss.c",
    "auth2-hostbased.c",
    "auth2-kbdint.c",
    "auth2-none.c",
    "auth2-passwd.c",
    "auth2-pubkey.c",
    "auth2-pubkeyfile.c",
    "auth2.c",
    "groupaccess.c",
    "gss-serv-krb5.c",
    "gss-serv.c",
    "loginrec.c",
    "monitor.c",
    "monitor_wrap.c",
    "platform.c",
    "sandbox-capsicum.c",
    "sandbox-darwin.c",
    "sandbox-null.c",
    "sandbox-pledge.c",
    "sandbox-rlimit.c",
    "sandbox-seccomp-filter.c",
    "sandbox-solaris.c",
    "sandbox-systrace.c",
    "servconf.c",
    "serverloop.c",
    "session.c",
    "sftp-common.c",
    "sftp-server.c",
    "srclimit.c",
    "sshd.c",
    "sshlogin.c",
    "sshpty.c",
    "uidswap.c",
  ]
  configs += [ ":fuchsia" ]
  deps = [
    ":libssh",
    ":sk",
  ]
}

executable("ssh-keygen") {
  sources = [
    "ssh-keygen.c",
    "sshsig.c",
  ]
  configs += [ ":fuchsia" ]
  deps = [
    ":libssh",
    ":sk",
  ]
}

executable("hostkeygen") {
  configs += [ ":fuchsia" ]
  sources = [ "fuchsia/hostkeygen/hostkeygen.c" ]
  deps = [
    ":libssh",
    ":sk",
  ]
}

executable("sftp-server") {
  sources = [
    "sftp-common.c",
    "sftp-server-main.c",
    "sftp-server.c",
  ]
  configs += [ ":fuchsia" ]
  deps = [ ":libssh" ]
}

executable("scp") {
  sources = [
    "progressmeter.c",
    "scp.c",
  ]
  configs += [ ":fuchsia" ]
  deps = [
    ":libssh",
    ":sftp-client",
  ]
}
